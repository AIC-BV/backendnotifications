<?php namespace StudioBosco\BackendNotifications\Workflows;

use StudioBosco\Workflows\Classes\NodeManager;
use StudioBosco\BackendNotifications\Workflows\Nodes\NotifyNode;

class Workflows
{
    public static function register()
    {
        NodeManager::registerNodes([
            NotifyNode::class,
        ]);
    }
}
