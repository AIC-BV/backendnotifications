<?php namespace StudioBosco\BackendNotifications\Workflows\Nodes;

use Event;
use BackendAuth;
use Backend\Models\User;
use StudioBosco\Workflows\Nodes\Node;

class NotifyNode extends Node
{
    public $name = 'studiobosco.backendnotifications.notify';

    public function nodeDetails(): array
    {
        return [
            'title' => trans('studiobosco.backendnotifications::workflows.nodes.notify.title'),
            'description' => trans('studiobosco.backendnotifications::workflows.nodes.notify.description'),
            'group' => trans('studiobosco.backendnotifications::lang.plugin.name'),
        ];
    }

    public function defineInputs():array
    {
        return [
            'trigger' => [
                'label' => trans('studiobosco.workflows::node.inputs.trigger'),
                'comment' => trans('studiobosco.workflows::node.inputs.trigger_comment'),
                'type' => 'none',
            ],
            'subject' => [
                'label' => trans('studiobosco.backendnotifications::workflows.inputs.subject'),
            ],
            'body' => [
                'label' => trans('studiobosco.backendnotifications::workflows.inputs.body'),
                'type' => 'textarea',
            ],
            'url' => [
                'label' => trans('studiobosco.backendnotifications::workflows.inputs.url'),
            ],
            'user_id' => [
                'label' => trans('studiobosco.backendnotifications::workflows.inputs.user_id'),
            ],
            'user' => [
                'label' => trans('studiobosco.backendnotifications::workflows.inputs.user'),
                'type' => 'none',
            ],
        ];
    }

    public function process($callback = null)
    {
        $subject = $this->getInput('subject');
        $body = $this->getInput('body');
        $url = $this->getInput('url');
        $userId = $this->getInput('user_id');
        $user = $this->getInput('user');

        if (!$user && $userId) {
            $user = User::find($userId);
        }

        if (!$user && !$userId) {
            $user = BackendAuth::getUser();
        }

        if (!$user) {
            return;
        }

        Event::fire('studiobosco.backendnotifications.notify', [$user, $subject, $body, $url]);

        return parent::process($callback);
    }
}
