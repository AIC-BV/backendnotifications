<?php return [
    'nodes' => [
        'notify' => [
            'title' => 'Backend notification',
            'description' => 'Sends as backend notification',
        ],
    ],
    'inputs' => [
        'user_id' => 'User ID',
        'user' => 'User',
        'subject' => 'Subject',
        'body' => 'Body',
        'url' => 'URL',
    ],
];
