# WinterCMS Backend Notifications Plugin

This plugin provides an API and UI for sending notifications to backend users.

## Usage

The plugin provides a new entry in the backend navigation that shows the number of notifications the user has and on the page it lists all notifications. If allowed by the user, it will make use of browser push-notifications.
The plugin will poll for new notifications every 5 seconds.

Notifications can be created programatically by firing an event:

```php
Event::fire('studiobosco.backendnotifications.notify', [$user, $subject, $body, $url]);
```
- `$user` can be a user ID or a backend user object
- `$subject` is mandatory and a string representing the notification subject
- `$body` is optional and a string representing the html formatted body text of the notification
- `$url` is optional and a string represting the URL to which the notification should link to.
