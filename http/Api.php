<?php
namespace StudioBosco\BackendNotifications\Http;

use Config;
use Input;
use Carbon\Carbon;
use Backend\Classes\Controller;
use StudioBosco\BackendNotifications\Helpers\BackendNotifications;

class Api extends Controller
{
    public function preferences()
    {
        return response()->json([
            'data' => BackendNotifications::getPreferences()->toArray()
        ], 200);
    }

    public function count()
    {
        return response()->json([
            'data' => BackendNotifications::getCount()
        ], 200);
    }

    public function latest()
    {
        $date = Input::get('date');
        if (!$date) {
            return response()->json([
                'error' => 'Missing date.',
            ], 400);
        }

        $date = new Carbon($date);
        $date->tz = Config::get('app.timezone');

        return response()->json([
            'data' => BackendNotifications::getLatestSince($date)->toArray()
        ], 200);
    }
}
